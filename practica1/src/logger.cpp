#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main(int argc, char* argv[]){

	mkfifo("/tmp/fifo1", 0777);
	int fd=open("/tmp/fifo1",O_RDWR);
	if(fd<0) perror("ERROR loggermain");

	while(1){
		char cad[200];
		read(fd, cad, sizeof(cad));
		printf("%s\n", cad);
	}
	
	close(fd);
	unlink("/tmp/fifo1");
	return 0;
}
