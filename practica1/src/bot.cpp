#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>




int main(){
	
	//Variable de tipo puntero a DatosMemCompartida
	DatosMemCompartida *Mem;
	//Abrir el archivo
	int fd2=open("/tmp/Bot", O_RDWR);
	if(fd2<0) perror("ERROR Botmain");

	//Proyectar el archivo en memoria (mmap)
	Mem=(DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);

	//cerrar el archivo
	close(fd2);

	//condiciones
	while(1){
		
		if(Mem->raqueta1.y1 < Mem->esfera.centro.y) Mem->accion=1;
		else if (Mem->raqueta1.y2 > Mem->esfera.centro.y) Mem->accion=-1;
		
		//suspenda durante 25ms
		usleep(25000);
	}
	
return 0;
}

	
